const User = require("../models/User");
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require("bcrypt");
const auth = require('../auth');


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// Register User
module.exports.registerUser = async (reqBody) => {

	return User.findOne({$or: [{email: reqBody.email}, {contactNo: reqBody.contactNo}]})
	.then(result => {
		if(!result){
			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				contactNo : reqBody.contactNo,
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return {duplicate: false};
				}
			})
		}
		else{
			if(result.email == reqBody.email){
				return {duplicate: true, reason: 'email'};
			}
			else{
				return {duplicate: true, reason: 'mobile'};
			}
		}
	})

	
}



// User login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email})
	.then(result => {
		if(!result){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

 


// Set admin
module.exports.setAdmin = async (data, isAdmin) => {

	if(isAdmin){
		let update = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(data, update)
		.then((promise, error) => {
			if(error){
				return false;
			}
			else{
				return `User has been given admin permission`;
			}
		})
	}

	else{
		return `Admin authority only`;
	}
}
